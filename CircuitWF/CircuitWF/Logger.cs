
using System;
using System.IO;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

/**
 * 
 */
namespace power
{
    public interface ILogger
    {

        void Log(string message);
        void ClearLog();
        void Log(AbstractApplience app);
        void Log(Line ln);
        void Log(Circuit cs);
    }

    public class ConsoleLogger : ILogger
    {
        public void Log(string message)
        {
            Console.WriteLine(message);
        }

        public void Log(AbstractApplience app)
        {
            this.Log("\n-------- Start of Applience --------");
            this.Log("Name\t\t\t" + app.GetName());
            this.Log("Type\t\t\t" + app.GetType());
            this.Log("Voltage\t\t\t" + app.GetInputVoltage());
            this.Log("Frequency\t\t" + app.GetInputFrequency());
            this.Log("Wattage\t\t\t" + app.GetWattage());
            this.Log("Line\t\t\t" + app.GetLineOfConnection());
            this.Log("Name of Line\t\t" + app.GetLineOfConnection().GetName());
            this.Log("--------- End of Applience ---------");
        }

        public void Log(Line ln)
        {
            this.Log("\n******** Start of Line ********");
            this.Log("Name\t\t\t" + ln.GetName());
            this.Log("Voltage\t\t\t" + ln.GetVoltage());
            this.Log("Frequency\t\t" + ln.GetFrequency());
            this.Log("Maximum Wattage\t\t" + ln.GetMaximumWattage());
            this.Log("Current Wattage\t\t" + ln.GetCurrentWattage());
            this.Log("Circuit\t\t\t" + ln.GetCurrentCircuit());
            this.Log("Name of Circuit\t\t" + ln.GetCurrentCircuit().GetName());
            this.Log("Connected Applience: ");

            foreach (AbstractApplience app in ln.GetListOfConnectedApplience())
                this.Log(app);

            this.Log("\n********* End of Line *********");
        }

        public void Log(Circuit cs)
        {
            this.Log("\n########## Start of Circuit ##########");
            this.Log("Name\t\t\t" + cs.GetName());
            this.Log("Current Wattage\t\t" + cs.CalculateCurrentWattage());
            this.Log("Included Line:");

            foreach (Line ln in cs.GetListOfLines())
                this.Log(ln);

            this.Log("\n########### End of Circuit ###########");
        }
        public void ClearLog() { }
    }





    public class FileLogger : ILogger
    {
        private string _path;
        public FileLogger()
        {
            _path = @"C:\Users\������\Desktop\�����\���������� ����������������\lab_3.1\log.txt";
        }

        public FileLogger(string Path)
        {
            _path = Path;
        }

        public void Log(string message)
        {
            
            if (!File.Exists(_path))
            {
                using (var tw = new StreamWriter(_path, true))
                {
                    tw.WriteLine(message);
                    tw.Close();
                }
            }
            else
            using (var tw = new StreamWriter(_path, true))
            {
                tw.WriteLine(message);
                tw.Close();
            }
             
        }

        public void Log(AbstractApplience app)
        {
            this.Log("\n-------- Start of Applience --------");
            this.Log("Name\t\t\t" + app.GetName());
            this.Log("Type\t\t\t" + app.GetType());
            this.Log("Voltage\t\t\t" + app.GetInputVoltage());
            this.Log("Frequency\t\t" + app.GetInputFrequency());
            this.Log("Wattage\t\t\t" + app.GetWattage());
            this.Log("Line\t\t\t" + app.GetLineOfConnection());
            this.Log("Name of Line\t" + app.GetLineOfConnection().GetName());
            this.Log("--------- End of Applience ---------");
        }

        public void Log(Line ln)
        {
            this.Log("\n******** Start of Line ********");
            this.Log("Name\t\t\t" + ln.GetName());
            this.Log("Voltage\t\t\t" + ln.GetVoltage());
            this.Log("Frequency\t\t" + ln.GetFrequency());
            this.Log("Maximum Wattage\t" + ln.GetMaximumWattage());
            this.Log("Current Wattage\t" + ln.GetCurrentWattage());
            this.Log("Circuit\t\t\t" + ln.GetCurrentCircuit());
            this.Log("Name of Circuit\t" + ln.GetCurrentCircuit().GetName());
            this.Log("Connected Applience: ");

            foreach (AbstractApplience app in ln.GetListOfConnectedApplience())
                this.Log(app);

            this.Log("\n********* End of Line *********");
        }

        public void Log(Circuit cs)
        {
            this.Log("\n########## Start of Circuit ##########");
            this.Log("Name\t\t\t\t" + cs.GetName());
            this.Log("Current Wattage\t\t" + cs.CalculateCurrentWattage());
            this.Log("Included Line:");

            foreach (Line ln in cs.GetListOfLines())
                this.Log(ln);

            this.Log("\n########### End of Circuit ###########");
        }

        public void ClearLog()
        {
            File.Delete(_path);
        }
    }

    public class LoggerFactory
    {

       static public ILogger CreateLogger(char type)
        {

            if (type == 'f') { FileLogger fl = new FileLogger(); return fl; }
            if (type == 'c') { ConsoleLogger fl = new ConsoleLogger(); return fl; }
            else { FileLogger fl = new FileLogger(); return fl; }

        }

    }
}