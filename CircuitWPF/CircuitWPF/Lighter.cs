
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/**
 * 
 */
namespace power
{
    public class Lighter : AbstractApplience
    {

        /**
         * 
         */
        public Lighter(string name, int inputVoltage, int wattage, int inputFrequency) :
            base(name, inputVoltage, wattage, inputFrequency)
        {
        }

        public Lighter(string name, int inputVoltage, int wattage, int inputFrequency, int areaOfLighting) :
            base(name, inputVoltage, wattage, inputFrequency)
        {
            _areaOfLighting = areaOfLighting;
        }

        public Lighter( int inputVoltage, int wattage, int inputFrequency) :
            base(inputVoltage, wattage, inputFrequency)
        {
        }

        /**
         * 
         */
        private int _areaOfLighting;

        /**
         * 
         */
        public void SetAreaOfLighting(int areaOfLighting)
        {
            _areaOfLighting = areaOfLighting;
        }

        /**
         * 
         */
        public int GetAreaOfLighting()
        {
            return _areaOfLighting;
        }

    }
}