
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/**
 * 
 */
namespace power
{
    public class CPU : AbstractApplience
    {

        /**
         * 
         */
        public CPU(string name, int inputVoltage, int wattage, int inputFrequency) :
            base(name, inputVoltage, wattage, inputFrequency)
        {
        }

        public CPU(string name, int inputVoltage, int wattage, int inputFrequency, long perfomance, long flashSize) :
            base(name, inputVoltage, wattage, inputFrequency)
        {
            _perfomance = perfomance;
            _flashSize = flashSize;
        }

        public CPU( int inputVoltage, int wattage, int inputFrequency) :
            base(inputVoltage, wattage, inputFrequency)
        {
        }

        /**
         * 
         */
        private long _perfomance;


        private long _flashSize;


        public void SetPerfomance(long perfomance)
        {
            _perfomance = perfomance;
        }

        /**
         * 
         */
        public long GetPerfomance()
        {
            return _perfomance;
        }

        /**
         * 
         */
        public void SetFlashSize(long flashSize)
        {
            _flashSize = flashSize;
        }

        /**
         * 
         */
        public long GetFlashSize()
        {
            return _flashSize;
        }

    }
}