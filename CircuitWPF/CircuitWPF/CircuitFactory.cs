
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/**
 * 
 */
namespace power{
public class CircuitFactory {

    /**
     * 
     */
    public static Circuit CreatEmptyCircuit() {
    //    ILogger il = LoggerFactory.CreateLogger('f');
        Circuit circuit = new Circuit("defaultName");
      //  il.Log("\n---\t was Created Empty "+ circuit.GetName() + " Circuit\t---\n");
        return circuit;
    }

    public static Circuit CreatEmptyCircuit(string name)
    {
      //  ILogger il = LoggerFactory.CreateLogger('f');
        Circuit circuit = new Circuit(name);
     //   il.Log("\n---\t was Created Empty " + circuit.GetName() + " Circuit\t---\n");
        return circuit;
    }

    public static Circuit CreatFullCircuit()
    {
      //  ILogger il = LoggerFactory.CreateLogger('f');
        Circuit circuit = new Circuit();
        Random rand = new Random();
        int capacity = rand.Next(1, 11);
    //    il.Log("\n---\t was Created Full Circuit for\t" + capacity + " lines\t---\n");

        for (int i = 0; i < capacity; i++)
        {
      //      il.Log("Line " + i);
            circuit.AddLine(LineFactory.CreateFullLine());

        }
     //   il.Log("\n---\t Current Wattage of this Circuit is \t" + circuit.CalculateCurrentWattage() + "\t---\n");
        return circuit;
    }

    public static Circuit CreatFullCircuit(ushort a)
    {
      //  ILogger il = LoggerFactory.CreateLogger('f');
        Circuit circuit = new Circuit();
        int capacity = a;
      //  il.Log("\n---\t was Created Full Circuit for\t" + capacity + " lines\t---\n");

        for (int i = 0; i < capacity; i++)
        {
         //   il.Log("Line " + i);
            circuit.AddLine(LineFactory.CreateFullLine());

        }
     //   il.Log("\n---\t Current Wattage of this Circuit is \t" + circuit.CalculateCurrentWattage() + "\t---\n");
        return circuit;
    }
}
}