﻿using System;
/*using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 * */
using System.Windows.Forms;
using power;
namespace CircuitWF
{
    public partial class NewApplience : Form
    {
        Line _ln;
        AbstractApplience app;
        public NewApplience(Line ln)
        {
            _ln = ln;
            InitializeComponent();
            typeComboBox.SelectedIndex = 0;
            voltageTextBox.Text = Convert.ToString(ln.GetVoltage());
            frequencyTextBox.Text = Convert.ToString(ln.GetFrequency());
        }

        private void isRandomCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (this.isRandomCheckBox.Checked)
            {
                nameTextBox.Enabled = false;
                voltageTextBox.Enabled = false;
                frequencyTextBox.Enabled = false;
                wattageTextBox.Enabled = false;
                typeComboBox.Enabled = false;
            }

            else
            {
                nameTextBox.Enabled = true;
                voltageTextBox.Enabled = true;
                frequencyTextBox.Enabled = true;
                wattageTextBox.Enabled = true;
                typeComboBox.Enabled = true;
            }
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            if (isRandomCheckBox.Checked)
            {
                if (_ln.ConnectApplience(ApplienceFactory.CreateRandomApplience()))
                {
                    MessageBox.Show("Applience connected succesfully");
                    this.Close();
                }

                else
                {
                    MessageBox.Show("Failed");
                }
            }
            else
            {
                int a = typeComboBox.SelectedIndex;
                char t = 'H';
                switch (a) 
                {
                    case 0: t = 'H'; break;
                    case 1: t = 'L'; break;
                    case 2: t = 'C'; break;
                    case 3: t = 'O'; break;

                }
                app = (ApplienceFactory.CreateConcreteApplience(Convert.ToInt32(voltageTextBox.Text),
                    Convert.ToInt32(wattageTextBox.Text), Convert.ToInt32(frequencyTextBox.Text), t));
                app.SetName(nameTextBox.Text);

                if (_ln.ConnectApplience(app))
                {
                    MessageBox.Show("Applience connected succesfully");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Failed");
                }
            }
        }
    }
}
