
//using System;
using System.Collections.Generic;
//using System.Linq;
//using System.Text;
using System.Windows.Forms;
/**
 * 
 */
namespace power
{
    public class Filler
    {

        /**
         * 
         */
        private ListBox _ListOfCircuits;
        private ListBox _ListOfLines;
        private ListBox _ListOfApplience;

        public Filler(ListBox ListOfCircuits, ListBox ListOfLines, ListBox ListOfApplience)
        {
            _ListOfApplience = ListOfApplience;
            _ListOfCircuits = ListOfCircuits;
            _ListOfLines = ListOfLines;
        }

        public void Fill<T>(List<T> listOfThings, ListBox listBox)
        {
            if (listOfThings != null && listBox != null)
            {
                listBox.Items.Clear();
                foreach (T t in listOfThings)
                {
                    listBox.Items.Add(t);
                }
            }
        }

        public void Fill(Circuit cs)
        {
            if (cs != null && _ListOfLines != null)
            {
                _ListOfLines.Items.Clear();
                foreach (Line ln in cs.GetListOfLines())
                {
                    _ListOfLines.Items.Add(ln);
                }
            }

            else
                if (_ListOfLines != null)
                    _ListOfLines.Items.Clear();
        }

        public void Fill(Line ln)
        {
            if (ln != null && _ListOfApplience != null)
            {
                _ListOfApplience.Items.Clear();
                foreach (AbstractApplience app in ln.GetListOfConnectedApplience())
                {
                    _ListOfApplience.Items.Add(app);
                }
            }
            else
                if (_ListOfApplience != null)
                    _ListOfApplience.Items.Clear();
        }

        public void Refresh()
        {
            
            Fill(_ListOfCircuits.SelectedItem as Circuit);
            Fill(_ListOfLines.SelectedItem as Line);
            
        }

    }
}