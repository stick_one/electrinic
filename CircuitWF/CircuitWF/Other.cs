/*
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
 * */

/**
 * 
 */
namespace power
{
    public class Other : AbstractApplience
    {

        /**
         * 
         */
        public Other(string name, int inputVoltage, int wattage, int inputFrequency) :
            base(name, inputVoltage, wattage, inputFrequency)
        {
        }

        public Other(string name, int inputVoltage, int wattage, int inputFrequency, string specialization) :
            base(name, inputVoltage, wattage, inputFrequency)
        {
            _specilization = specialization;
        }

        public Other( int inputVoltage, int wattage, int inputFrequency) :
            base(inputVoltage, wattage, inputFrequency)
        {
        }

        /**
         * 
         */
        private string _specilization;

        /**
         * 
         */
        public void SetSpecialization(string specialization)
        {
            _specilization = specialization;
        }

        /**
         * 
         */
        public string GetSpecialization()
        {
            return _specilization;
        }

    }
}