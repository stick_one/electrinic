﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
/*
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 * */
using System.Windows.Forms;
using power;

namespace CircuitWF
{
    public partial class NewCircuit : Form
    {
        List<Circuit> _mylist;
        public NewCircuit(List<Circuit> myList)
        {
            _mylist = myList;
            InitializeComponent();

        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            if (isFullCheckBox.Checked)
            {
                _mylist.Add(CircuitFactory.CreatFullCircuit());
                this.Close();
            }
            else
            {
                _mylist.Add(CircuitFactory.CreatEmptyCircuit(nameTextBox.Text));
                this.Close();

            }
        }

        private void isFullCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (isFullCheckBox.Checked)
            {
                nameTextBox.Enabled = false;
            }
            else
            {
                nameTextBox.Enabled = true;
            }
        }
    }
}
