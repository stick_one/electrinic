
using System;
/*
using System.Collections.Generic;
using System.Linq;
using System.Text;
 * */
using System.Windows.Forms;
/**
 * 
 */
namespace power
{
    public class Viewer
    {
        private Control _CName;
        private Control _CWattage;

        private Control _LName;
        private Control _LMaximumWattage;
        private Control _LCurrentWattage;
        private Control _LVoltage;
        private Control _LFrequency;

        private Control _AName;
        private Control _AWattage;
        private Control _AFrequency;
        private Control _AVoltage;
        private Control _AType;

        private ListBox _CListBox;
        private ListBox _LListBox;
        private ListBox _AListBox;


        public Viewer(Control CName, Control CWattage, Control LName, Control LMaximumWattage, Control LCurrentWattage,
            Control LVoltage, Control LFrequency, Control AName, Control AWattage, Control AVoltage, Control AFrequency,
            Control AType, ListBox CListBox, ListBox LListBox,  ListBox AListBox)
        {
            _CName = CName;
            _CWattage = CWattage;

            _LName = LName;
            _LMaximumWattage = LMaximumWattage;
            _LCurrentWattage = LCurrentWattage;
            _LVoltage = LVoltage;
            _LFrequency = LFrequency;

            _AName = AName;
            _AWattage = AWattage;
            _AVoltage = AVoltage;
            _AFrequency = AFrequency;
            _AType = AType;

            _CListBox = CListBox;
            _LListBox = LListBox;
            _AListBox = AListBox;

        }
        /**
         * 
         */
        public void Show(Circuit cs)
        {
            if (cs != null && _CName != null && _CWattage != null)
            {
                _CName.Text = cs.GetName();
                _CWattage.Text = Convert.ToString(cs.CalculateCurrentWattage());
            }

            else
                if (_CName != null && _CWattage != null)
                {
                    _CName.Text = "";
                    _CWattage.Text = "";
                }
        }

        public void Show(Line ln)
        {
            if (ln != null && _LName != null && _LMaximumWattage != null && _LCurrentWattage != null && _LVoltage != null 
                && _LFrequency != null)
            {
                _LName.Text = ln.GetName();
                _LMaximumWattage.Text = Convert.ToString(ln.GetMaximumWattage());
                _LCurrentWattage.Text = Convert.ToString(ln.GetCurrentWattage());
                _LVoltage.Text = Convert.ToString(ln.GetVoltage());
                _LFrequency.Text = Convert.ToString(ln.GetFrequency());
            }
            else
                if (_LName != null && _LMaximumWattage != null && _LCurrentWattage != null && _LVoltage != null 
                    && _LFrequency != null)
                {
                    _LName.Text = "";
                    _LMaximumWattage.Text = "";
                    _LCurrentWattage.Text = "";
                    _LVoltage.Text = "";
                    _LFrequency.Text = "";
                }
        }

        public void Show(AbstractApplience app)
        {
            if (app != null && _AName != null && _AWattage != null && _AWattage != null && _AVoltage != null 
                && _AFrequency != null && _AType != null)
            {
                _AName.Text = app.GetName();
                _AWattage.Text = Convert.ToString(app.GetWattage());
                _AVoltage.Text = Convert.ToString(app.GetInputVoltage());
                _AFrequency.Text = Convert.ToString(app.GetInputFrequency());
                _AType.Text = Convert.ToString(app.GetType());
            }
            else
                if (_AName != null && _AWattage != null && _AWattage != null && _AVoltage != null && _AFrequency != null 
                    && _AType != null)
                {
                    _AName.Text = "";
                    _AWattage.Text = "";
                    _AVoltage.Text = "";
                    _AFrequency.Text = "";
                    _AType.Text = "";
                }
        }

        public void Refresh()
        {
            Show(_CListBox.SelectedItem as Circuit);
            Show(_LListBox.SelectedItem as Line);
            Show(_AListBox.SelectedItem as AbstractApplience);
        }

    }
}