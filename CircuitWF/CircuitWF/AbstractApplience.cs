
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/**
 * 
 */
namespace power
{
    public class AbstractApplience
    {

        public AbstractApplience(int inputVoltage, int wattage, int inputFrequency)
        {
            this.SetInputVoltage(inputVoltage);
            this.SetWattage(wattage);
            this.SetInputFrequency(inputFrequency);
        }

        public AbstractApplience(string name, int inputVoltage, int wattage, int inputFrequency)
        {
            this.SetName(name);
            this.SetInputVoltage(inputVoltage);
            this.SetWattage(wattage);
            this.SetInputFrequency(inputFrequency);
        }

        
        protected string _name = "DefaultApplience'sName";

        private int _inputVoltage = 0;

        private int _wattage = 0;

        private int _inputFrequency = 0;

        private Line _lineOfConnection;


        //������� ���������� ������ ������, ������ ���� ���������� ��������� �� �����
        public override string ToString()
        {
            return _name;
        }

        public bool SetName(string name)
        {
            if (this.GetConnectedStatus() == false)
            {
                _name = name;
                return true;
            }
            return false;
        }

        /*
        public override string ToString()
        {
            return _name;
        }
        */
        public string GetName()
        {
            return _name;
        }

        public bool SetInputVoltage(int inputVoltage)
        {
            if (inputVoltage > 0 && this.GetConnectedStatus() == false)
            {
                _inputVoltage = inputVoltage;
                return true;
            }
            return false;
        }


        public int GetInputVoltage()
        {
            return _inputVoltage;
        }


        public bool SetWattage(int wattage)
        {
            if (wattage > 0 && this.GetConnectedStatus() == false)
            {
                _wattage = wattage;
                return true;
            }
            return false;
        }


        public int GetWattage()
        {
            return _wattage;
        }


        public bool SetInputFrequency(int inputFrequency)
        {
            if (inputFrequency < 1000 && inputFrequency >= 0 && this.GetConnectedStatus() == false)
            {
                _inputFrequency = inputFrequency;
                return true;
            }
            return false;
        }


        public int GetInputFrequency()
        {
            return _inputFrequency;
        }

        public bool SetLineOfConnection(Line myLine) //��������� ������� ���� � ������ ������������ ���������, ������������� �������
            //����� ���� ����� ����, ��������� ���� ���������� � ������� �����, � �������� ��
        {
            if (_lineOfConnection == null)
            {
                if (Checker(myLine) == true)
                    _lineOfConnection = myLine;
                return true;
            }
            else
            {
                if (Checker(_lineOfConnection) == false && myLine == null)
                {
                    _lineOfConnection = null;
                    return true;
                }
            }

            return false;
        }



        public Line GetLineOfConnection()
        {
            return _lineOfConnection;
        }


        
        public bool GetConnectedStatus() // ���� ������� ����� ����, ������ ���������� ���������
        {
            if(_lineOfConnection != null)
                return true;
            return false;
        }


        private bool Checker(Line lineForCheck) //��������� ������� ���� � ������ ������������ ��������� � �����
        {
            if(lineForCheck != null)
            foreach (AbstractApplience ap in lineForCheck.GetListOfConnectedApplience())
                if (ap == this)
                    return true;

            return false;
        }
    }
}