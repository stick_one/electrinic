﻿using System;
using System.Collections.Generic;
/*
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 * */
using System.Windows.Forms;
using power;

namespace CircuitWF
{
    public partial class NewLine : Form
    {
        Circuit _cs;
        public NewLine(Circuit cs)
        {
            _cs = cs;
            InitializeComponent();
        }


        private void isFullCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (this.isFullCheckBox.Checked)
            {
                nameTextBox.Enabled = false;
                voltageTextBox.Enabled = false;
                frequencyTextBox.Enabled = false;
                wattageTextBox.Enabled = false;
            }

            else
            {
                nameTextBox.Enabled = true;
                voltageTextBox.Enabled = true;
                frequencyTextBox.Enabled = true;
                wattageTextBox.Enabled = true;
            }
        }


        private void connectButton_Click(object sender, EventArgs e)
        {
            if (isFullCheckBox.Checked)
            {
                if (_cs.AddLine(LineFactory.CreateFullLine()))
                {
                    MessageBox.Show("Line created succesfully");
                    this.Close();
                }

                else
                {
                    MessageBox.Show("Failed");
                }
            }
            else
            {
                Line ln = LineFactory.CreateEmptyLine(Convert.ToInt32(voltageTextBox.Text), Convert.ToInt32(wattageTextBox.Text),
                    Convert.ToInt32(frequencyTextBox.Text));
                ln.SetName(nameTextBox.Text);

                if (_cs.AddLine(ln))
                {
                    MessageBox.Show("Line created succesfully");
                    this.Close();
                }

                else
                {
                    MessageBox.Show("Failed");
                }
            }
        }
    }
}
