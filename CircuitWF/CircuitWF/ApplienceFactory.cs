
using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

/**
 * 
 */
namespace power
{
    public class ApplienceFactory
    {

        public static AbstractApplience CreateRandomApplience()
        {
            Random rand = new Random();
            int type = rand.Next(0,4);
            AbstractApplience applience = new AbstractApplience(220, 100, 50);
           // ILogger il = LoggerFactory.CreateLogger('f');
            switch (type)
            {
                case 0:
                    {
                        applience = new CPU(220, rand.Next(50, 100), 50);
                       // il.Log("was created \t CPU \t\t v220 \t w" + applience.GetWattage() + " \t f50");
                    }
                    break;
                case 1:
                    {
                        applience = new Heater(220, rand.Next(1000, 5000), 50);
                     //   il.Log("was created \t Heater \t v220 \t w" + applience.GetWattage() + " \t f50");
                    }
                    break;
                case 2:
                    {
                        applience = new Lighter(220, rand.Next(500, 1000), 50);
                     //   il.Log("was created \t Lighter \t v220 \t w" + applience.GetWattage() + " \t f50");
                    }
                    break;
                case 3:
                    {
                        applience = new Other(220, rand.Next(50, 1000), 50);
                    //    il.Log("was created \t Other \t\t v220 \t w" + applience.GetWattage() + " \t f50");
                    }
                    break;

            }

            return applience;
        }

        public static AbstractApplience CreateRandomApplience(int voltage, int wattage, int frequency)
        {
            Random rand = new Random();
            int type = rand.Next(0, 4);
            AbstractApplience applience = new AbstractApplience(voltage, wattage, frequency);
           // ILogger il = LoggerFactory.CreateLogger('f');
            switch (type)
            {
                case 0:
                    {
                        applience = new CPU(voltage, wattage, frequency);
                     //   il.Log("was created \t CPU \t\t v" + voltage + "\t w" + wattage + " \t f" + frequency);
                    }
                    break;
                case 1:
                    {
                        applience = new Heater(voltage, wattage, frequency);
                     //   il.Log("was created \t Heater \t v" + voltage + "\t w" + wattage + " \t f" + frequency);
                    }
                    break;
                case 2:
                    {
                        applience = new Lighter(voltage, wattage, frequency);
                     //   il.Log("was created \t Lighter \t v" + voltage + "\t w" + wattage + " \t f" + frequency);
                    }
                    break;
                case 3:
                    {
                        applience = new Other(voltage, wattage, frequency);
                     //   il.Log("was created \t Other \t\t v" + voltage + "\t w" + wattage + " \t f" + frequency);
                    }
                    break;

            }

            return applience;
        }

        public static AbstractApplience CreateConcreteApplience(int voltage, int wattage, int frequency, char type)
        {
            AbstractApplience applience = new AbstractApplience(voltage, wattage, frequency);
           // ILogger il = LoggerFactory.CreateLogger('f');
            switch (type)
            {
                case 'C':
                    {
                        applience = new CPU(voltage, wattage, frequency);
                      //  il.Log("was created \t CPU \t\t v" + voltage + "\t w" + wattage + " \t f" + frequency);
                    }
                    break;
                case 'H':
                    {
                        applience = new Heater(voltage, wattage, frequency);
                      //  il.Log("was created \t Heater \t v" + voltage + "\t w" + wattage + " \t f" + frequency);
                    }
                    break;
                case 'L':
                    {
                        applience = new Lighter(voltage, wattage, frequency);
                      //  il.Log("was created \t Lighter \t v" + voltage + "\t w" + wattage + " \t f" + frequency);
                    }
                    break;
                case 'O':
                    {
                        applience = new Other(voltage, wattage, frequency);
                     //   il.Log("was created \t Other \t\t v" + voltage + "\t w" + wattage + " \t f" + frequency);
                    }
                    break;

            }

            return applience;
        }
    }
}