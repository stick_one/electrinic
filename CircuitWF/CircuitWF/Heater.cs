
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

/**
 * 
 */
namespace power
{
    public class Heater : AbstractApplience
    {

        public Heater(string name, int inputVoltage, int wattage, int inputFrequency) : 
            base (name, inputVoltage, wattage, inputFrequency)
        {
        }

        public Heater(string name, int inputVoltage, int wattage, int inputFrequency, int weight, int areaOfHeating) :
            base(name, inputVoltage, wattage, inputFrequency)
        {
            _weight = weight;
            _areaOfHeating = areaOfHeating;
        }

        public Heater(int inputVoltage, int wattage, int inputFrequency) :
            base(inputVoltage, wattage, inputFrequency)
        {
        }

        /**
         * 
         */
        private int _weight;

        private int _areaOfHeating;



        public void SetWeight(int weight)
        {
            _weight = weight;
        }

        /**
         * 
         */
        public int GetWeight()
        {
            return _weight;
        }

        /**
         * 
         */
        public void SetAreaOfHeating(int areaOfHeating)
        {
            _areaOfHeating = areaOfHeating;
        }

        /**
         * 
         */
        public int GetAreaOfHeating()
        {
            return _areaOfHeating;
        }

    }
}