
//using System;
using System.Collections.Generic;
//using System.Linq;
//using System.Text;

/**
 * 
 */
namespace power
{
    public class Line
    {

        public Line(int outputVoltage, int maximumWattage, int frequency)
        {
            _outputVoltage = outputVoltage;
            _maximumWattage = maximumWattage;
            _frequency = frequency;
        }

        public Line(string name, int outputVoltage, int maximumWattage, int frequency)
        {
            this.SetName(name);
            this.SetVoltage(outputVoltage);
            this.SetMaximumWattage(maximumWattage);
            this.SetFrequency(frequency);
        }


        private string _name = "DefaultLine'sName";

        private Circuit _currentCircuit;

        private int _outputVoltage = 0;

        private int _currentWattage = 0;

        private int _maximumWattage = 0;

        private int _frequency = 0;

        private List<AbstractApplience> _listOfConnectedApplience = new List<AbstractApplience>();

        //������� ���������� ������ ������, ������ ���� ������ ������������ ��������� ����.

        public bool SetName(string name)
        {
            _name = name;
            if (_name == name)
                return true;
            return false;
        }


        public override string ToString()
        {
            return _name;
        }

        public string GetName()
        {
            return _name;
        }


        public bool SetVoltage(int voltage)
        {
            if (_listOfConnectedApplience.Count == 0 && voltage > 0)
            {
                _outputVoltage = voltage;
                return true;
            }
            return false;
        }


        public int GetVoltage()
        {
            return _outputVoltage;
        }


        public bool SetMaximumWattage(int maximumWattage)
        {
            if (_listOfConnectedApplience.Count == 0 && maximumWattage > 0)
            {
                _maximumWattage = maximumWattage;
                return true;
            }
            return false;
        }


        public int GetMaximumWattage()
        {
            return _maximumWattage;
        }


        public bool SetFrequency(int frequency)
        {
            if (_listOfConnectedApplience.Count == 0 && frequency > 0)
            {
                _frequency = frequency;
                return true;
            }
            return false;
        }


        public int GetFrequency()
        {
            return _frequency;
        }

        public int GetCurrentWattage()
        {
            return _currentWattage;
        }

        public List<AbstractApplience> GetListOfConnectedApplience()
        {
            return _listOfConnectedApplience;
        }


        public bool ConnectApplience(AbstractApplience applience)
            //��������� ������������ ����������, ���� � ���������� ������� ���� ����, ��������� ���������� � ������ ������������
            //���� �� �������, ������� �� ������, ���� ������� ����������� ������� ��������
        {
            if (applience.GetInputFrequency() == _frequency && applience.GetInputVoltage() == _outputVoltage
                && _maximumWattage - _currentWattage >= applience.GetWattage())

                if (applience.GetLineOfConnection() == null)
                {
                    _listOfConnectedApplience.Add(applience);
                    if (applience.SetLineOfConnection(this))
                    {
                        _currentWattage += applience.GetWattage();
                        return true;
                    }
                    else
                    {
                        _listOfConnectedApplience.Remove(applience);
                        return false;
                    }

                }
            return false;
        }


        public bool DisconnectApplience(AbstractApplience applience) // ������� ����� ���������� �� ������, �������� 
            //����� ���������� ����������, ���� �� ������� ���������� ��� � ������ ������������
        {
            _listOfConnectedApplience.Remove(applience);
            if (applience.SetLineOfConnection(null))
            {
                _currentWattage -= applience.GetWattage();
                return true;
            }
            else
            {
                _listOfConnectedApplience.Add(applience);
                return false;
            }
        }

        public Circuit GetCurrentCircuit()
        {
            return _currentCircuit;
        }

        public bool SetCurrentCircuit(Circuit circuit) // ��������� ������� ���� � ������ ����� � ����, ������������� ������� ����
            //���� ���� ����� ����, ��������� ���������� ���� � ������ ������� ����, ���� �������, ������� ������� ����
        {
            if (circuit != null)
            {
                if (_currentCircuit == null && Checker(circuit))
                {
                    _currentCircuit = circuit;
                    return true;
                }

            }
            else
            {
                if (_currentCircuit != null && Checker(_currentCircuit) == false)
                {
                    _currentCircuit = null;
                    return true;
                }
            }
            return false;
        }

        private bool Checker(Circuit circuit) // ��������� ������� ����� � ������ ����� � ����
        {
            if (circuit != null)
                foreach (Line ln in circuit.GetListOfLines())
                    if (ln == this)
                        return true;

            return false;
        }
    }
}