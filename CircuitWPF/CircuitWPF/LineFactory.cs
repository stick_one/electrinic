
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/**
 * 
 */
namespace power
{
    public class LineFactory
    {
        public static Line CreateEmptyLine(int outputVoltage, int maximumWattage, int frequency)
        {
         //   ILogger il = LoggerFactory.CreateLogger('f');
            Line ln = new Line(outputVoltage, maximumWattage, frequency);
        //    il.Log("");
        //    il.Log("***\twas created Empty Line\t v" + ln.GetVoltage() + " \t w" + ln.GetMaximumWattage() + "\t f" +
          //      ln.GetFrequency() + "\t***");
         //   il.Log("");
            return ln;

        }

        public static Line CreateEmptyLine()
        {
            Random rand = new Random();
         //   ILogger il = LoggerFactory.CreateLogger('f');
            Line ln = new Line(220, rand.Next(5000, 20001), 50);
          //  il.Log("");
          //  il.Log("***\twas created Empty Line\t v" + ln.GetVoltage() + " \t w" + ln.GetMaximumWattage() + "\t f" + 
           //     ln.GetFrequency() + "\t***");
          //  il.Log("");
            return ln;

        }

        public static Line CreateFullLine(int outputVoltage, int maximumWattage, int frequency)
        {
            AbstractApplience app;
          //  ILogger il = LoggerFactory.CreateLogger('f');
            Line ln = new Line(outputVoltage, maximumWattage, frequency);
          //  il.Log("");
         //   il.Log("***\twas created Full Line\t v" + ln.GetVoltage() + " \t w" + ln.GetMaximumWattage() + "\t f" +
         //       ln.GetFrequency() + "\t***");
         //   il.Log("");

            app = ApplienceFactory.CreateRandomApplience();
            while (ln.ConnectApplience(app))
            {
            //    il.Log("And Connected");
                app = ApplienceFactory.CreateRandomApplience();
            }

          //  il.Log("And deleted");
         //   il.Log("");

       //     il.Log("Current wattage of this line is\t" + ln.GetCurrentWattage());

        //    il.Log("");
            return ln;

        }

        public static Line CreateFullLine()
        {
            Random rand = new Random();
            AbstractApplience app;
          //  ILogger il = LoggerFactory.CreateLogger('f');
            Line ln = new Line(220, rand.Next(5000, 20001), 50);
           // il.Log("");
           // il.Log("***\twas created Full Line\t v" + ln.GetVoltage() + " \t w" + ln.GetMaximumWattage() + "\t f" +
             //   ln.GetFrequency() + "\t***");
           // il.Log("");

            app = ApplienceFactory.CreateRandomApplience();
            while (ln.ConnectApplience(app))
            {
             //   il.Log("And Connected");
                app = ApplienceFactory.CreateRandomApplience();
            }

           // il.Log("And deleted");
          //  il.Log("");

           // il.Log("Current wattage of this line is\t" + ln.GetCurrentWattage());

          //  il.Log("");
            return ln;

        }

    }
}