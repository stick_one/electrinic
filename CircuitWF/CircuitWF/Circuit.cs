
//using System;
using System.Collections.Generic;
//using System.Linq;
//using System.Text;

/**
 * 
 */
namespace power{
public class Circuit {

    /**
     * 
     */
    public Circuit() {
    }

    public Circuit(string name)
    {
        this.SetName(name);
    }

    private string _name = "DefaultCircuit'sName";

    private long _currentWattage = 0;

    private List<Line> _listOfLines = new List<Line>();


    public override string ToString()
    {
        return _name;
    }

    public bool SetName(string name) 
    {
       _name = name;
        return true;
    }

    public string GetName() 
    {
        return _name;
    }

    public bool AddLine(Line line) //��������� ����� ����� � ����, ��������� ����� � ������, �������� ����� ��������� �����,
        //���� �� ������� - ������� ����� �� ������
    {
        if (line != null && line.GetCurrentCircuit() == null)
        {
            _listOfLines.Add(line);
            if (line.SetCurrentCircuit(this))
            {
                return true;
            }
            _listOfLines.Remove(line);
        }
        return false;
    }


    public List<Line> GetListOfLines() 
    {
        return _listOfLines;
    }


    public bool RemoveLine(Line line) //������� ����� �� ������, �������� ����� �������� �����, ���� �� ������� 
        //��������� ����� � ������
    {
        if (line != null)
        {
            _listOfLines.Remove(line);

            if (line.SetCurrentCircuit(null))
            {
                return true;
            }
            _listOfLines.Add(line);
        }

        return false;
    }


    public long CalculateCurrentWattage() 
    {
        _currentWattage = 0;
        foreach (Line ln in _listOfLines)
            _currentWattage += ln.GetCurrentWattage();
                return _currentWattage;
    }
}
}