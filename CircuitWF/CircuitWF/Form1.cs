﻿using System;
using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using power;
namespace CircuitWF
{
    public partial class Form1 : Form
    {
        Viewer vw;
        Filler fl;
        List<Circuit> listOfCircuits = new List<Circuit>(); 

        public Form1()
        {
            listOfCircuits.Add(CircuitFactory.CreatFullCircuit());
            listOfCircuits.Add(CircuitFactory.CreatFullCircuit());
            listOfCircuits.Add(CircuitFactory.CreatFullCircuit());

            listOfCircuits[0].SetName("serj1");
            listOfCircuits[1].SetName("serj2");
            listOfCircuits[2].SetName("serj3");
            InitializeComponent();

            vw = new Viewer(CName, CWattage, LName, LMaximumWattage, LWattage, LVoltage, LFrequency, AName, AWattage,
             AVoltage, AFrequency, AType, listBoxOfCircuits, listBoxOfLines, listBoxOfApplience);

            fl = new Filler(listBoxOfCircuits, listBoxOfLines, listBoxOfApplience);

            fl.Fill<Circuit>(listOfCircuits, listBoxOfCircuits);

        }

        private void listOfCircuits_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            vw.Show(listBoxOfCircuits.SelectedItem as Circuit);
            fl.Fill(listBoxOfCircuits.SelectedItem as Circuit);
        }

        private void listBoxOfLines_SelectedIndexChanged(object sender, EventArgs e)
        {
            vw.Show(listBoxOfLines.SelectedItem as Line);
            fl.Fill(listBoxOfLines.SelectedItem as Line);
        }





        private void listBoxOfApplience_SelectedIndexChanged(object sender, EventArgs e)
        {
            
                vw.Show(listBoxOfApplience.SelectedItem as AbstractApplience);
        }




        private void ADisconnect_Click(object sender, EventArgs e)
        {
            if (listBoxOfApplience.SelectedItem as AbstractApplience != null)
            {
                (listBoxOfApplience.SelectedItem as AbstractApplience).GetLineOfConnection().DisconnectApplience(listBoxOfApplience.SelectedItem as AbstractApplience);
                /*
                fl.Fill(listBoxOfLines.SelectedItem as Line);
                vw.Show(listBoxOfLines.SelectedItem as Line);
                vw.Show(listBoxOfCircuits.SelectedItem as Circuit);
                 * */
                fl.Refresh();
                vw.Refresh();
            }
        }


        private void LRemove_Click(object sender, EventArgs e)
        {
            if (listBoxOfLines.SelectedItem as Line != null)
            {
                (listBoxOfLines.SelectedItem as Line).GetCurrentCircuit().RemoveLine(listBoxOfLines.SelectedItem as Line);
                fl.Refresh();
                vw.Refresh();
            }
        }

        private void CRemove_Click(object sender, EventArgs e)
        {
            if (listBoxOfCircuits.SelectedItem != null)
            {
                listOfCircuits.Remove(listBoxOfCircuits.SelectedItem as Circuit);
                fl.Fill<Circuit>(listOfCircuits, listBoxOfCircuits);
                fl.Refresh();
                vw.Refresh();
            }
        }

        private void ANew_Click(object sender, EventArgs e)
        {
            if (listBoxOfLines.SelectedItem as Line != null)
            {
                NewApplience na = new NewApplience(listBoxOfLines.SelectedItem as Line);
                na.Show();
            }
            else
            {
                MessageBox.Show("Error: Line is null\nChoose correct Line");
            }
        }

        private void LNew_Click(object sender, EventArgs e)
        {
            if (listBoxOfCircuits.SelectedItem as Circuit != null)
            {
                NewLine nl = new NewLine(listBoxOfCircuits.SelectedItem as Circuit);
                nl.Show();
            }
            else
            {
                MessageBox.Show("Error: Circuit is null\nChoose correct Circuit");
            }
        }

        private void CNew_Click(object sender, EventArgs e)
        {
            NewCircuit nc = new NewCircuit(listOfCircuits);
            nc.Show();
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            fl.Fill<Circuit>(listOfCircuits, listBoxOfCircuits);
        }

    }
}
