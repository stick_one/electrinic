﻿namespace CircuitWF
{
    partial class NewApplience
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.isRandomCheckBox = new System.Windows.Forms.CheckBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.voltageTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.frequencyTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.wattageTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.typeComboBox = new System.Windows.Forms.ComboBox();
            this.connectButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // isRandomCheckBox
            // 
            this.isRandomCheckBox.AutoSize = true;
            this.isRandomCheckBox.Location = new System.Drawing.Point(12, 12);
            this.isRandomCheckBox.Name = "isRandomCheckBox";
            this.isRandomCheckBox.Size = new System.Drawing.Size(177, 24);
            this.isRandomCheckBox.TabIndex = 0;
            this.isRandomCheckBox.Text = "isRandomApplience";
            this.isRandomCheckBox.UseVisualStyleBackColor = true;
            this.isRandomCheckBox.CheckedChanged += new System.EventHandler(this.isRandomCheckBox_CheckedChanged);
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(12, 42);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(220, 26);
            this.nameTextBox.TabIndex = 1;
            this.nameTextBox.Text = "DefaultApplience\'sName";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(258, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(258, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Input Voltage";
            // 
            // voltageTextBox
            // 
            this.voltageTextBox.Location = new System.Drawing.Point(12, 74);
            this.voltageTextBox.Name = "voltageTextBox";
            this.voltageTextBox.Size = new System.Drawing.Size(220, 26);
            this.voltageTextBox.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(258, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Input Frequency";
            // 
            // frequencyTextBox
            // 
            this.frequencyTextBox.Location = new System.Drawing.Point(12, 106);
            this.frequencyTextBox.Name = "frequencyTextBox";
            this.frequencyTextBox.Size = new System.Drawing.Size(220, 26);
            this.frequencyTextBox.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(258, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Wattage";
            // 
            // wattageTextBox
            // 
            this.wattageTextBox.Location = new System.Drawing.Point(12, 138);
            this.wattageTextBox.Name = "wattageTextBox";
            this.wattageTextBox.Size = new System.Drawing.Size(220, 26);
            this.wattageTextBox.TabIndex = 7;
            this.wattageTextBox.Text = "1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(258, 173);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "Type";
            // 
            // typeComboBox
            // 
            this.typeComboBox.FormattingEnabled = true;
            this.typeComboBox.Items.AddRange(new object[] {
            "Heater",
            "Lighter",
            "CPU",
            "Other"});
            this.typeComboBox.Location = new System.Drawing.Point(12, 173);
            this.typeComboBox.Name = "typeComboBox";
            this.typeComboBox.Size = new System.Drawing.Size(220, 28);
            this.typeComboBox.TabIndex = 11;
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(12, 207);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(371, 51);
            this.connectButton.TabIndex = 12;
            this.connectButton.Text = "Try to Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // NewApplience
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 271);
            this.Controls.Add(this.connectButton);
            this.Controls.Add(this.typeComboBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.wattageTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.frequencyTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.voltageTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.isRandomCheckBox);
            this.Name = "NewApplience";
            this.Text = "NewApplience";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox isRandomCheckBox;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox voltageTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox frequencyTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox wattageTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox typeComboBox;
        private System.Windows.Forms.Button connectButton;
    }
}