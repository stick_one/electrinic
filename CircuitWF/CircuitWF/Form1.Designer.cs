﻿namespace CircuitWF
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxOfCircuits = new System.Windows.Forms.ListBox();
            this.listBoxOfLines = new System.Windows.Forms.ListBox();
            this.listBoxOfApplience = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.LFrequency = new System.Windows.Forms.Label();
            this.LVoltage = new System.Windows.Forms.Label();
            this.LWattage = new System.Windows.Forms.Label();
            this.LName = new System.Windows.Forms.Label();
            this.LMaximumWattage = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.AType = new System.Windows.Forms.Label();
            this.AFrequency = new System.Windows.Forms.Label();
            this.AVoltage = new System.Windows.Forms.Label();
            this.AWattage = new System.Windows.Forms.Label();
            this.AName = new System.Windows.Forms.Label();
            this.CWattage = new System.Windows.Forms.Label();
            this.CName = new System.Windows.Forms.Label();
            this.ADisconnect = new System.Windows.Forms.Button();
            this.LRemove = new System.Windows.Forms.Button();
            this.CRemove = new System.Windows.Forms.Button();
            this.ANew = new System.Windows.Forms.Button();
            this.LNew = new System.Windows.Forms.Button();
            this.CNew = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBoxOfCircuits
            // 
            this.listBoxOfCircuits.FormattingEnabled = true;
            this.listBoxOfCircuits.ItemHeight = 20;
            this.listBoxOfCircuits.Location = new System.Drawing.Point(51, 66);
            this.listBoxOfCircuits.Name = "listBoxOfCircuits";
            this.listBoxOfCircuits.Size = new System.Drawing.Size(241, 244);
            this.listBoxOfCircuits.TabIndex = 0;
            this.listBoxOfCircuits.SelectedIndexChanged += new System.EventHandler(this.listOfCircuits_SelectedIndexChanged);
            // 
            // listBoxOfLines
            // 
            this.listBoxOfLines.FormattingEnabled = true;
            this.listBoxOfLines.ItemHeight = 20;
            this.listBoxOfLines.Location = new System.Drawing.Point(440, 66);
            this.listBoxOfLines.Name = "listBoxOfLines";
            this.listBoxOfLines.Size = new System.Drawing.Size(273, 244);
            this.listBoxOfLines.TabIndex = 1;
            this.listBoxOfLines.SelectedIndexChanged += new System.EventHandler(this.listBoxOfLines_SelectedIndexChanged);
            // 
            // listBoxOfApplience
            // 
            this.listBoxOfApplience.FormattingEnabled = true;
            this.listBoxOfApplience.ItemHeight = 20;
            this.listBoxOfApplience.Location = new System.Drawing.Point(866, 66);
            this.listBoxOfApplience.Name = "listBoxOfApplience";
            this.listBoxOfApplience.Size = new System.Drawing.Size(270, 244);
            this.listBoxOfApplience.TabIndex = 2;
            this.listBoxOfApplience.SelectedIndexChanged += new System.EventHandler(this.listBoxOfApplience_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(141, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Circuits";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(549, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Lines";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(961, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Appliences";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(47, 330);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(436, 330);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 20);
            this.label5.TabIndex = 7;
            this.label5.Text = "Name";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(862, 330);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 20);
            this.label6.TabIndex = 8;
            this.label6.Text = "Name";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(47, 363);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 20);
            this.label7.TabIndex = 9;
            this.label7.Text = "Total Wattage";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(436, 393);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(127, 20);
            this.label8.TabIndex = 10;
            this.label8.Text = "Current Wattage";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(862, 363);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 20);
            this.label9.TabIndex = 11;
            this.label9.Text = "Wattage";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(436, 423);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 20);
            this.label10.TabIndex = 12;
            this.label10.Text = "Voltage";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(862, 393);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 20);
            this.label11.TabIndex = 13;
            this.label11.Text = "Voltage";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(436, 454);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 20);
            this.label12.TabIndex = 14;
            this.label12.Text = "Frequency";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(862, 424);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 20);
            this.label13.TabIndex = 15;
            this.label13.Text = "Frequency";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(862, 454);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 20);
            this.label14.TabIndex = 16;
            this.label14.Text = "Type";
            // 
            // LFrequency
            // 
            this.LFrequency.AutoSize = true;
            this.LFrequency.Location = new System.Drawing.Point(611, 454);
            this.LFrequency.Name = "LFrequency";
            this.LFrequency.Size = new System.Drawing.Size(84, 20);
            this.LFrequency.TabIndex = 20;
            this.LFrequency.Text = "Frequency";
            // 
            // LVoltage
            // 
            this.LVoltage.AutoSize = true;
            this.LVoltage.Location = new System.Drawing.Point(611, 423);
            this.LVoltage.Name = "LVoltage";
            this.LVoltage.Size = new System.Drawing.Size(64, 20);
            this.LVoltage.TabIndex = 19;
            this.LVoltage.Text = "Voltage";
            // 
            // LWattage
            // 
            this.LWattage.AutoSize = true;
            this.LWattage.Location = new System.Drawing.Point(611, 393);
            this.LWattage.Name = "LWattage";
            this.LWattage.Size = new System.Drawing.Size(127, 20);
            this.LWattage.TabIndex = 18;
            this.LWattage.Text = "Current Wattage";
            // 
            // LName
            // 
            this.LName.AutoSize = true;
            this.LName.Location = new System.Drawing.Point(611, 330);
            this.LName.Name = "LName";
            this.LName.Size = new System.Drawing.Size(51, 20);
            this.LName.TabIndex = 17;
            this.LName.Text = "Name";
            // 
            // LMaximumWattage
            // 
            this.LMaximumWattage.AutoSize = true;
            this.LMaximumWattage.Location = new System.Drawing.Point(611, 363);
            this.LMaximumWattage.Name = "LMaximumWattage";
            this.LMaximumWattage.Size = new System.Drawing.Size(141, 20);
            this.LMaximumWattage.TabIndex = 22;
            this.LMaximumWattage.Text = "Maximum Wattage";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(436, 363);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(141, 20);
            this.label16.TabIndex = 21;
            this.label16.Text = "Maximum Wattage";
            // 
            // AType
            // 
            this.AType.AutoSize = true;
            this.AType.Location = new System.Drawing.Point(1037, 454);
            this.AType.Name = "AType";
            this.AType.Size = new System.Drawing.Size(43, 20);
            this.AType.TabIndex = 27;
            this.AType.Text = "Type";
            // 
            // AFrequency
            // 
            this.AFrequency.AutoSize = true;
            this.AFrequency.Location = new System.Drawing.Point(1037, 424);
            this.AFrequency.Name = "AFrequency";
            this.AFrequency.Size = new System.Drawing.Size(84, 20);
            this.AFrequency.TabIndex = 26;
            this.AFrequency.Text = "Frequency";
            // 
            // AVoltage
            // 
            this.AVoltage.AutoSize = true;
            this.AVoltage.Location = new System.Drawing.Point(1037, 393);
            this.AVoltage.Name = "AVoltage";
            this.AVoltage.Size = new System.Drawing.Size(64, 20);
            this.AVoltage.TabIndex = 25;
            this.AVoltage.Text = "Voltage";
            // 
            // AWattage
            // 
            this.AWattage.AutoSize = true;
            this.AWattage.Location = new System.Drawing.Point(1037, 363);
            this.AWattage.Name = "AWattage";
            this.AWattage.Size = new System.Drawing.Size(70, 20);
            this.AWattage.TabIndex = 24;
            this.AWattage.Text = "Wattage";
            // 
            // AName
            // 
            this.AName.AutoSize = true;
            this.AName.Location = new System.Drawing.Point(1037, 330);
            this.AName.Name = "AName";
            this.AName.Size = new System.Drawing.Size(51, 20);
            this.AName.TabIndex = 23;
            this.AName.Text = "Name";
            // 
            // CWattage
            // 
            this.CWattage.AutoSize = true;
            this.CWattage.Location = new System.Drawing.Point(209, 363);
            this.CWattage.Name = "CWattage";
            this.CWattage.Size = new System.Drawing.Size(109, 20);
            this.CWattage.TabIndex = 29;
            this.CWattage.Text = "Total Wattage";
            // 
            // CName
            // 
            this.CName.AutoSize = true;
            this.CName.Location = new System.Drawing.Point(209, 330);
            this.CName.Name = "CName";
            this.CName.Size = new System.Drawing.Size(51, 20);
            this.CName.TabIndex = 28;
            this.CName.Text = "Name";
            // 
            // ADisconnect
            // 
            this.ADisconnect.Location = new System.Drawing.Point(1142, 66);
            this.ADisconnect.Name = "ADisconnect";
            this.ADisconnect.Size = new System.Drawing.Size(103, 37);
            this.ADisconnect.TabIndex = 30;
            this.ADisconnect.Text = "Disconnect";
            this.ADisconnect.UseVisualStyleBackColor = true;
            this.ADisconnect.Click += new System.EventHandler(this.ADisconnect_Click);
            // 
            // LRemove
            // 
            this.LRemove.Location = new System.Drawing.Point(719, 66);
            this.LRemove.Name = "LRemove";
            this.LRemove.Size = new System.Drawing.Size(103, 37);
            this.LRemove.TabIndex = 31;
            this.LRemove.Text = "Remove";
            this.LRemove.UseVisualStyleBackColor = true;
            this.LRemove.Click += new System.EventHandler(this.LRemove_Click);
            // 
            // CRemove
            // 
            this.CRemove.Location = new System.Drawing.Point(298, 66);
            this.CRemove.Name = "CRemove";
            this.CRemove.Size = new System.Drawing.Size(103, 37);
            this.CRemove.TabIndex = 32;
            this.CRemove.Text = "Remove";
            this.CRemove.UseVisualStyleBackColor = true;
            this.CRemove.Click += new System.EventHandler(this.CRemove_Click);
            // 
            // ANew
            // 
            this.ANew.Location = new System.Drawing.Point(1142, 109);
            this.ANew.Name = "ANew";
            this.ANew.Size = new System.Drawing.Size(103, 37);
            this.ANew.TabIndex = 33;
            this.ANew.Text = "New";
            this.ANew.UseVisualStyleBackColor = true;
            this.ANew.Click += new System.EventHandler(this.ANew_Click);
            // 
            // LNew
            // 
            this.LNew.Location = new System.Drawing.Point(719, 109);
            this.LNew.Name = "LNew";
            this.LNew.Size = new System.Drawing.Size(103, 37);
            this.LNew.TabIndex = 34;
            this.LNew.Text = "New";
            this.LNew.UseVisualStyleBackColor = true;
            this.LNew.Click += new System.EventHandler(this.LNew_Click);
            // 
            // CNew
            // 
            this.CNew.Location = new System.Drawing.Point(298, 109);
            this.CNew.Name = "CNew";
            this.CNew.Size = new System.Drawing.Size(103, 37);
            this.CNew.TabIndex = 35;
            this.CNew.Text = "New";
            this.CNew.UseVisualStyleBackColor = true;
            this.CNew.Click += new System.EventHandler(this.CNew_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1912, 708);
            this.Controls.Add(this.CNew);
            this.Controls.Add(this.LNew);
            this.Controls.Add(this.ANew);
            this.Controls.Add(this.CRemove);
            this.Controls.Add(this.LRemove);
            this.Controls.Add(this.ADisconnect);
            this.Controls.Add(this.CWattage);
            this.Controls.Add(this.CName);
            this.Controls.Add(this.AType);
            this.Controls.Add(this.AFrequency);
            this.Controls.Add(this.AVoltage);
            this.Controls.Add(this.AWattage);
            this.Controls.Add(this.AName);
            this.Controls.Add(this.LMaximumWattage);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.LFrequency);
            this.Controls.Add(this.LVoltage);
            this.Controls.Add(this.LWattage);
            this.Controls.Add(this.LName);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBoxOfApplience);
            this.Controls.Add(this.listBoxOfLines);
            this.Controls.Add(this.listBoxOfCircuits);
            this.Name = "Form1";
            this.Text = "WattageCalculator";
            this.Activated += new System.EventHandler(this.Form1_Activated);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxOfCircuits;
        private System.Windows.Forms.ListBox listBoxOfLines;
        private System.Windows.Forms.ListBox listBoxOfApplience;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label LFrequency;
        private System.Windows.Forms.Label LVoltage;
        private System.Windows.Forms.Label LWattage;
        private System.Windows.Forms.Label LName;
        private System.Windows.Forms.Label LMaximumWattage;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label AType;
        private System.Windows.Forms.Label AFrequency;
        private System.Windows.Forms.Label AVoltage;
        private System.Windows.Forms.Label AWattage;
        private System.Windows.Forms.Label AName;
        private System.Windows.Forms.Label CWattage;
        private System.Windows.Forms.Label CName;
        private System.Windows.Forms.Button ADisconnect;
        private System.Windows.Forms.Button LRemove;
        private System.Windows.Forms.Button CRemove;
        private System.Windows.Forms.Button ANew;
        private System.Windows.Forms.Button LNew;
        private System.Windows.Forms.Button CNew;
    }
}

