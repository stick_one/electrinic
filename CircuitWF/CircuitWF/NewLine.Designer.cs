﻿namespace CircuitWF
{
    partial class NewLine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.connectButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.wattageTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.frequencyTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.voltageTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.isFullCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(12, 170);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(371, 51);
            this.connectButton.TabIndex = 24;
            this.connectButton.Text = "Create";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(258, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 20);
            this.label4.TabIndex = 21;
            this.label4.Text = "Maximum Wattage";
            // 
            // wattageTextBox
            // 
            this.wattageTextBox.Location = new System.Drawing.Point(12, 138);
            this.wattageTextBox.Name = "wattageTextBox";
            this.wattageTextBox.Size = new System.Drawing.Size(220, 26);
            this.wattageTextBox.TabIndex = 20;
            this.wattageTextBox.Text = "1000";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(258, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(137, 20);
            this.label3.TabIndex = 19;
            this.label3.Text = "Output Frequency";
            // 
            // frequencyTextBox
            // 
            this.frequencyTextBox.Location = new System.Drawing.Point(12, 106);
            this.frequencyTextBox.Name = "frequencyTextBox";
            this.frequencyTextBox.Size = new System.Drawing.Size(220, 26);
            this.frequencyTextBox.TabIndex = 18;
            this.frequencyTextBox.Text = "50";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(258, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 20);
            this.label2.TabIndex = 17;
            this.label2.Text = "Output Voltage";
            // 
            // voltageTextBox
            // 
            this.voltageTextBox.Location = new System.Drawing.Point(12, 74);
            this.voltageTextBox.Name = "voltageTextBox";
            this.voltageTextBox.Size = new System.Drawing.Size(220, 26);
            this.voltageTextBox.TabIndex = 16;
            this.voltageTextBox.Text = "220";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(258, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 15;
            this.label1.Text = "Name";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(12, 42);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(220, 26);
            this.nameTextBox.TabIndex = 14;
            this.nameTextBox.Text = "DefaultLine\'sName";
            // 
            // isFullCheckBox
            // 
            this.isFullCheckBox.AutoSize = true;
            this.isFullCheckBox.Location = new System.Drawing.Point(12, 12);
            this.isFullCheckBox.Name = "isFullCheckBox";
            this.isFullCheckBox.Size = new System.Drawing.Size(99, 24);
            this.isFullCheckBox.TabIndex = 13;
            this.isFullCheckBox.Text = "isFull line";
            this.isFullCheckBox.UseVisualStyleBackColor = true;
            this.isFullCheckBox.CheckedChanged += new System.EventHandler(this.isFullCheckBox_CheckedChanged);
            // 
            // NewLine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 254);
            this.Controls.Add(this.connectButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.wattageTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.frequencyTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.voltageTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.isFullCheckBox);
            this.Name = "NewLine";
            this.Text = "NewLine";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox wattageTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox frequencyTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox voltageTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.CheckBox isFullCheckBox;

    }
}